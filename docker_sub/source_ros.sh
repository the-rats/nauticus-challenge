#!/bin/bash

. /opt/ros/$ROS_DISTRO/setup.bash
. install/setup.bash
echo ["Welcome to ROS"]

ros2 run simple_sub listener
exec "$@"
